//Khai báo biến:
// Bài 1:
var salary_per_day = 100000;
var work_day;
var total_salary;
var element_p1;
var text1;

//Bài 2:
var _num;
var count = 0;
var _string = [];
var _average = 0;
var count_of_enter = 0;
var element_p2;
var text2;

//Bài 3:
var ratio = 23500;
var USD;
var VND;
var element_p3;
var text3;

//Bài 4:
var _length;
var _width;
var area;
var premeter;
var element_p4;
var text4;

//Bài 5:
var _number;
var _num1;
var _num2;
var _sum;
var element_p5;
var text5;

// Có thể thêm hàm isNaN để kiểm tra xem dữ liệu nhập vào có phải là 1 số.
// kiểm tra số lớn hơn 0

//*Hàm:
// Bài 1:
// Get the input field: enter chỉ hoạt động khi truy cập vào input
var input1 = document.getElementById("day_input");
// Execute a function when the user presses a key on the keyboard
// If the user presses the "Enter" key on the keyboard
input1.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("myBtn1").click();
  }
});

function calculate_total_salary() {
  work_day = document.getElementById("day_input").value;
  total_salary = work_day * salary_per_day;
  text1 = "Tiền lương của bạn là: " + total_salary.toLocaleString() + " VNĐ.";
  element_p1 = document.getElementById("alert_1");
  element_p1.style.color = "black";
  element_p1.innerHTML = text1;
  return (document.getElementById("day_input").value = "");
}
// Bài 2:
// Get the input field: enter chỉ hoạt động khi truy cập vào input
var input2 = document.getElementById("number_input");
// Execute a function when the user presses a key on the keyboard
input2.addEventListener("keypress", function (event) {
  // If the user presses the "Enter" key on the keyboard
  if ((event.key === "Enter") & (count_of_enter < 6)) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("myBtn").click();
    count_of_enter++;
  }
  if ((event.key === "Enter") & (count_of_enter == 6)) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("myBtn2").click();
  }
});

function take_number_value() {
  _num = document.getElementById("number_input").value;
  count++;
  if (count < 6) {
    text2 = "Số thứ " + count + " của bạn là " + _num;
    _string.push(_num);
  }
  if (count == 6) {
    document.getElementById("myBtn2").click();
  }
  element_p2 = document.getElementById("alert_2");
  element_p2.style.color = "black";
  element_p2.innerHTML = text2;
  return (document.getElementById("number_input").value = "");
}
function calculate_average() {
  _average =
    (Number(_string[0]) +
      Number(_string[1]) +
      Number(_string[2]) +
      Number(_string[3]) +
      Number(_string[4])) /
    5;
  text2 =
    "Giá trị TB của 5 số " +
    Number(_string[0]) +
    ", " +
    Number(_string[1]) +
    ", " +
    Number(_string[2]) +
    ", " +
    Number(_string[3]) +
    ", " +
    Number(_string[4]) +
    " là: " +
    _average;
  element_p2 = document.getElementById("alert_2");
  element_p2.style.color = "black";
  element_p2.innerHTML = text2;
  if (count < 5) {
    text2 = "Chưa nhập đủ 5 số!";
    element_p2 = document.getElementById("alert_2");
    element_p2.style.color = "black";
    element_p2.innerHTML = text2;
  }
  return (
    (count_of_enter = 0),
    (count = 0),
    (_string.length = 0),
    (document.getElementById("number_input").value = "")
  );
}

// Bài 3:
// Get the input field: enter chỉ hoạt động khi truy cập vào input
var input3 = document.getElementById("usd_input");
// Execute a function when the user presses a key on the keyboard
// If the user presses the "Enter" key on the keyboard
input3.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("myBtn3").click();
  }
});
function exchange() {
  USD = document.getElementById("usd_input").value;
  VND = USD * ratio;
  // text3=  "Tương đương với:";
  text3 = USD + "$ tương đương với " + VND.toLocaleString() + " VNĐ.";
  element_p3 = document.getElementById("thongbao3");
  element_p3.style.color = "black";
  element_p3.innerHTML = text3;
  return (document.getElementById("usd_input").value = "");
}
// Bài 4:
// Get the input field: enter chỉ hoạt động khi truy cập vào input
var input4_1 = document.getElementById("length");
// Execute a function when the user presses a key on the keyboard
// If the user presses the "Enter" key on the keyboard
input4_1.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    //  _length = document.getElementById('length').value ;
    document.getElementById("width").focus();
    // Trigger the button element with a click
    //  document.getElementById("myBtn3").click();
  }
});
var input4_2 = document.getElementById("width");
// Execute a function when the user presses a key on the keyboard
// If the user presses the "Enter" key on the keyboard
input4_2.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    calculate_area_and_premeter();
  }
});
function calculate_area_and_premeter() {
  _length = document.getElementById("length").value;
  _width = document.getElementById("width").value;
  if ((_length > 0) & (_width > 0)) {
    area = _length * _width;
    premeter = [Number(_length) + Number(_width)] * 2;
    text4 =
      "Diện tích và Chu vi hình chữ nhật lần lượt là: " +
      area +
      " và " +
      premeter;
  } else {
    text4 = "Vui lòng nhập lại!";
  }
  element_p4 = document.getElementById("alert_4");
  element_p4.style.color = "black";
  element_p4.innerHTML = text4;
  return (
    (document.getElementById("length").value = ""),
    (document.getElementById("width").value = "")
  );
}
//Bài 5
// Get the input field: enter chỉ hoạt động khi truy cập vào input
var input3 = document.getElementById("number");
// Execute a function when the user presses a key on the keyboard
// If the user presses the "Enter" key on the keyboard
input3.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("myBtn5").click();
  }
});
function sum_of_two_index() {
  _number = document.getElementById("number").value;
  if ((_number >= 10) & (_number <= 99)) {
    _num1 = Number(_number) / 10;
    _num1 = parseInt(_num1);
    _num2 = Number(_number) % 10;
    _sum = _num1 + _num2;
    text5 = "Tổng 2 ký số của số " + _number + " là " + _sum;
  } else {
    text5 = "Vui lòng nhập số 2 chữ số!";
  }
  element_p5 = document.getElementById("alert_5");
  element_p5.style.color = "black";
  element_p5.innerHTML = text5;
  return (document.getElementById("number").value = "");
}
